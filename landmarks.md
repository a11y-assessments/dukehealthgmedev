# ARIA Landmark issues

## Nested ARIA `<main>` Landmarks [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value) [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 2.4.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#bypass-blocks)

### Main Landmark duplicated.

The `<main>` landmark can only be used once per page. They also cannot be nested inside each other.

```html
<div id="body-wrap">
  <main>
    ...
  </main>
</div>   
```

#### Suggested solution:

Convert the outermost `<main>` to a `<div>`.

---

### `<nav>` Landmarks must be unique

Screen readers navigate pages using Landmarks. When there is a duplicate `<nav>` Landmark it needs an `aria-label` or `aria-labelledby` attribute to differentiate it from the other `<nav>` elements.

__#1 User utility menu__

__Visual location:__

![primary nav](assets/user-menu.png)

__HTML location:__

```html
<nav class="nav-user ">
  ...
</nav>
```

#### Suggested solution:

Add `aria-label="user utility nav"`.

```html
<nav class="nav-user" aria-label="user utility nav">
  ...
</nav>
```

---


__#2 Main menu__

__Visual location:__

![primary nav](assets/primary-nav.png)


__HTML location:__

```html
<nav id="primary-nav" aria-hidden="true">
  ...
</nav>
```

#### Suggested solution:

Add `aria-label="main menu"`

```html
<nav id="primary-nav" aria-label="main menu">
  ...
</nav>
```

---

