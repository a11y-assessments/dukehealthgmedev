# HTML semantics

## Inaccurate HTML semantics [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships)

### Page title is using `<blockquote>` element when the element is actually the page title.

__Visual location:__

![non semantic blockquote](assets/false-blockquote.png)


__Problematic HTML:__

```html
<blockquote class="testimonial">
  <h2>Welcome to Duke Graduate Medical Education</h2>
  <p>
    <a href="/training-programs" class="btn white clear">Details About Our Programs</a>
    <a href="/about/directors-welcome" class="btn white clear">Watch Video</a>
  </p>
</blockquote>
```

#### Suggested solution:

Change the page title to be an `<h1>`. Note it does not matter how it is styled. It can look like anything. It just needs to be semantic.

Use something like the following HTML without a `<blockquote>`:

`~` = change `-` = delete

```html
- <blockquote class="testimonial">
~  <h1>Welcome to Duke Graduate Medical Education</h1>
   <p>
     <a href="/training-programs" class="btn white clear">Details About Our Programs</a>
     <a href="/about/directors-welcome" class="btn white clear">Watch Video</a>
   </p>
- </blockquote>
```

_Note: Every page needs one `<h1>` this is the most appropriate content to be the title of this page. This solve two issues at the same time. This is not a problem on subpages._

<br>

---

<br>

### Empty headings in slideshow x4 (one for each slide)

Some users, especially keyboard and screen reader users, often navigate by heading elements. An empty heading will present no information and may introduce confusion.

__Visual location:__

_Slide 1 example:_

![H1 empty](assets/h1-prob-1.png)


__HTML location:__

```html
<div class="slide-background">
  <div class="mast-content">
    <span>
      <h1></h1>
      Lorem Ipsum Dolor Sit Amet
    </span>
  </div>
  <div class="slide-mask"></div>
</div>
```

#### Suggested solution:

__Part 1:__

Require that the content creators use the Title field provided in Drupal so the blank `<h1>` tags do not cause screen reader users confusion. 

__Part 2:__

There should only be one `<h1>` per page.  Heading 1 should be reserved to the page's title. These elements should be changed to `<h2>`.

<br>
