# Follow-up / recheck

Amazing remediation work. It is so refreshing to work with such competent and talented developers.  Just a couple very minor things remain.

Table of contents:

1. Unresolved issue
2. New issues
3. Concern / Question

## Unresolved issue

### Page title is using `<blockquote>` element when the element is actually the page title.

__Pleas see:__ <https://a11y-assessments.pages.oit.duke.edu/dukehealthgmedev/innaccurate-html-semantics.html>


<br>

---

<br>

## New issues

Training Programs page: <https://dukehealthgme.epicenter1.com/training-programs>

### Yellow Search input

Missing aria labels/screen reader text

__Visual location:__

![yellow search input with missing labels](assets/yellow-search-input.png)

__HTML location:__

```html
<input id="program-search" type="search" placeholder="Search Programs">
```

#### Suggested solution:

Apply the same fix that was successfully implemented on the search input <https://a11y-assessments.pages.oit.duke.edu/dukehealthgmedev/form-markup.html>


<br>

---

<br>


### Typo: `aria-contols` attribute should be `aria-controls`


```html
<button id="button-anesthesiology" 
  class="accordion-item-trigger" 
  aria-expanded="false" 
  aria-contols="section-anesthesiology">Anesthesiology
</button>
```

#### Suggested solution:

Fix the typo ;-)

<br>

---

<br>


### The `aria-labelledby` attribute is referencing an element that does not exist 

All pages: <https://dukehealthgme.epicenter1.com/>

![aria issue menu](assets/aria-labelledby-issue.png)

__HTML location:__

```html
<nav aria-labelledby="block-topnavigationmenu-menu" id="block-topnavigationmenu" aria-label="Top Navigation Menu">
```

#### Suggested solution:

The `aria-labelledby` attribute references an element that does not exist.  Since the element already has an accurate `aria-label`, it does not need the `aria-labelledby` attribute. 

Remove the `aria-labelledby` attribute.

<br>

---

<br>

## Concern / Question

### Slideshow

#### How will the slideshow be used with real content?

One of the issues as related to ensuring the slide indicators had enough contrast compared to the background.  Since the original assessment those have disappeared because there is only one slide.  See __"Slide indicators indistinguishable from background"__ on this page <https://a11y-assessments.pages.oit.duke.edu/dukehealthgmedev/color-contrast.html>. There is also an issue about `<h1>` heading tags. 

The only way to test this will be to see real/realistic content in the slideshow.

#### Suggested solution:

Add some real slideshow content so we can assess the slideshow capabilities.  






