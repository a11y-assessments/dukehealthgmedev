# Expand-collapse widget issue

## Interactive controls are not keyboard focusable [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard) and Custom controls are missing ARIA roles [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)


Description:<br>
If new content, such as a dialog, is added to the page, the user&#39;s focus is directed to it. [Learn more](https://web.dev/managed-focus/).

See page: __<https://dukehealthgme.epicenter1.com/training-programs>__

### Expand collapse not keyboard focusable and missing ARIA attributes to describe state

__Visual location:__

![expand collapse does not work](assets/expand-collapse.png)

Interactive components like this with show/hide functionality need to be accessible to keyboard-only and screen reader users.

This is by providing keyboard focus and ARIA attributes to describe the state of the object.

#### Suggested solution:

Change HTML to become keyboard focusable and add ARIA attributes. Similar to the following:

__Expanded:__

```html
<div class="accordion-item ordered active" data-index="3">
  <a href="#" aria-haspopup="true" aria-expanded="true" class="accordion-item-trigger"><h4>Dermatology</h4></a>
  <div class="accordion-item-content">
     ...
  </div>
</div>   
```

__Collapsed:__

```html
<div class="accordion-item ordered" data-index="3">
  <a href="#" aria-haspopup="true" aria-expanded="false" class="accordion-item-trigger"><h4>Dermatology</h4></a>
  <div class="accordion-item-content">
     ...
  </div>
</div>   
```

Add JS to toggle expand or collapsed state.
