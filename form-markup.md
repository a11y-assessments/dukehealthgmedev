# Search form markup issues

## Buttons do not have an accessible name [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

When a button doesn't have an accessible name, screen readers announce it as "button", making it unusable for users who rely on screen readers. [Learn more](https://web.dev/button-name/).


### This `button` or `input` has no `value` attribute or it contains no inner text to indicate its purpose

__Visual location:__

![button is not descriptive](assets/dukehealthgme-epicenter1-com-div-bound-layout-div-header-extras-form-header-search-button-button-submi.png)

#### HTML location:

```html
<button type="submit" class="button-submit">
  <span class="element-invisible">Search</span>
  <svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
    <path d=..." fill-rule="evenodd"></path>
  </svg>        
</button>
```

__Problem CSS:__

```css
.element-invisible {
	display: none;
}
```

The `.element-invisible` class does not have accurate properties.  It is using `display:none` in error.  It will completely hide the content both visually and also for screen reader users.  The end result is that the element does not functionally exist.

#### Suggested solution:

The contents of the `.element-invisible` class should be changed (or overridden) to:

```css
.element-invisible {
  display: block;
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0,0,0,0);
  border: 0;
}
```

This will hide it from sighted users, while allowing it to be recognized by assistive technology like screen readers.


<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`5,HTML,1,BODY,4,HEADER,0,DIV,1,DIV,0,FORM,1,BUTTON`<br>
Selector:<br>
`div.bound-layout > div.header-extras > form.header-search > button.button-submit`
</details>

<br>

---

<br>

## Form elements do not have associated labels [WCAG 3.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#labels-or-instructions)

Labels ensure that form controls are announced properly by assistive technologies, like screen readers. [Learn more](https://web.dev/label/).



### The Search element is missing a label

__Visual location:__

![Element missing label](assets/dukehealthgme-epicenter1-com-div-bound-layout-div-header-extras-form-header-search-input-input-searc.png)

__HTML location:__

```html
<input type="search" name="keyword" class="input-search" placeholder="Search">
```

`<inputs>` require some type of label. The `placeholder` attribute is not a compliant substitute although it is very helpful.


#### Suggested solution:

Chose one:

__Option 1:__

Add an `aria-label` attribute to the `<input>` element:

`+` = add.

```html
<input type="search" 
  name="keyword" 
  class="input-search" 
  placeholder="Search"
+ aria-label="Search">
```


__OR__

__Option 2 (more difficult):__

Add a `<label>` that is associated with the search `<input>`s ID.  This element does not have an `id` so it would also need added.  The end result should look something like this:

`+` = add.

```html
+ <label for="search1" class="element-invisible">
  <input type="search" 
    name="keyword" 
    class="input-search" 
    placeholder="Search"
+   id="search1">
```

__IMPORTANT NOTE__: _The class `.element-invisible`s attributes_ __must__ _be corrected as seen in previous task._



<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`5,HTML,1,BODY,4,HEADER,0,DIV,1,DIV,0,FORM,0,INPUT`<br>
Selector:<br>
`div.bound-layout > div.header-extras > form.header-search > input.input-search`
</details>



<br>

