## Keyboard `:focus` indicator missing on inputs  [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard) 

Inputs need to have a focus indicator when they can receive keyboard focus.  

Focusable descendents within a hidden element prevent those interactive elements from being available to users of assistive technologies like screen readers. [Learn more](https://web.dev/aria-hidden-focus/).


### Keyboard `:focus` indicator is missing on the search inputs and lost inside collapsed menu

__Please watch video explanation:__

<video width="640" controls>
  <source src="assets/gme_focus_indicator.mp4" type="video/mp4">
</video>

#### Suggested solution:

__Part 1:__

Add `:focus` indicator `outline`.

```css
.input-search:focus,
.button-submit:focus {
  outline: 1px solid #333;
}
```

__Part 2:__

Set hidden menu to `display:none` when it is off screen so the `:focus` indicator is not lost inside it on menu items.


