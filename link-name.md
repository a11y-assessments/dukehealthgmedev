# Logo missing accessible site name

## Links do not have a discernible name [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

Link text (and alternate text for images, when used as links) that is discernible, unique, and focusable improves the navigation experience for screen reader users. [Learn more](https://web.dev/link-name/).





### The `<a>` logo link has no text to identify its purpose

__Visual location:__

![ not descriptive](assets/dukehealthgme-epicenter1-com-header-header-div-bound-layout-div-logo-a-logo-lin.png)

__HTML Location__:

```html
<a href="/" class="logo-link">
```

Empty links are not read to a screen reader user, as a result, they will have no idea what the link does or where it would take them.

#### Suggested solution:

`+` = add.

```html
<a href="/" class="logo-link"
+ aria-label="Graduate Medical Education Home page">
```

<br>

_Additional note:_ `alt` was used, but it is not a valid attribute of the `<svg>` element. It is ignored by assistive technology. It could be removed.

```html
<svg viewBox="0 0 697 67" xmlns="http://www.w3.org/2000/svg" 
     alt="Duke Health Graduate Medical Education">
```

Other valid methods to add image descriptions:

<https://dequeuniversity.com/rules/axe/4.1/svg-img-alt>


<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Element is in tab order and does not have accessible text

Fix any of the following:
<br>Element does not have text that is visible to screen readers
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`5,HTML,1,BODY,4,HEADER,0,DIV,0,DIV,0,A`<br>
Selector:<br>
`header#header > div.bound-layout > div#logo > a.logo-link`
</details>

<br>
