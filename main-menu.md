# Main menu issues

## Interactive controls are not keyboard focusable [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard)

Description:<br>
Custom interactive controls are keyboard focusable and display a focus indicator. [Learn more](https://web.dev/focusable-controls/).

### Main menu not accessible to keyboard-only users

__Please watch video explanation:__

<video width="640" controls>
  <source src="assets/gme-nav-videos.mp4" type="video/mp4">
</video>

#### Suggested solution:

Make toggle button keyboard focusable with ability for the Enter key to toggle the sub-navigation items, as mentioned in the video.

<br>

---

<br>

## Custom controls are missing ARIA roles [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

Description:<br>
Custom interactive controls have appropriate ARIA roles. [Learn more](https://web.dev/custom-control-roles/).

### Main menu not accessible to screen reader users

__Please watch video explanation:__

<video width="640" controls>
  <source src="assets/gme_nav_videos_sr.mp4" type="video/mp4">
</video>

#### Suggested solution:

Ensure the sub-navigation's toggle button has `aria-expaned="true/false"`  attributes that accurately describe its current state or expanded or collapsed.

Add `aria-label` that states the purpose of the toggle element. Something like `aria-label="toggle sub nav"` would be fine, as long as it comes directly _after_ the menu item link. This way it is associated with that menu item based on proximity.

Example of using ARIA attributes for menu: <https://www.w3.org/WAI/tutorials/menus/flyout/#use-button-as-toggle>

Consider this Drupal 8 menu pattern for ideas <https://www.lullabot.com/articles/accessible-navigation-drupal-cores-menu-system>



---
