# Color contrast issues

## Background and foreground colors do not have a sufficient contrast ratio. [WCAG 1.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#contrast-minimum)

Low-contrast text is difficult or impossible for many users to read. [Learn more](https://web.dev/color-contrast/).

### Slide title headings

Background images can have light or dark areas. To ensure that white text can be legible on a light background we need to have a text-shadow or some other type of dark color behind the text.

__Visual location:__

![low contrast](assets/low-contrast-1.png)

__HTML location:__

```html
<div class="slide-background">
  <div class="mast-content">
    ...
  </div>
</div>
```

#### Suggested solution:

Add a compound drop shadow to the text

```css
.paragraph-hero_slider .slide-container .mast-content {
  text-shadow: 1px 1px 4px #000, -1px -1px 4px #000;
}
```

End result should have a fuzzy black drop shadow look like this:

![fixed low contrast](assets/low-contrast-1-fix.png)

---

### Slide indicators indistinguishable from background

Low-contrast elements are difficult or impossible for many users differenciate. [Learn more](https://web.dev/color-contrast/).

These dot indicators elements need more contrast.

__Visual location:__

_Best seen on slide 2_

![low contrast slide indicators](assets/slide-indicator-low-contrast.png)

__HTML location:__

```html
<ul class="slick-dots" style="" role="tablist">
  <li class="" role="presentation">...</li>
  <li role="presentation" class="slick-active">...</li>
  <li role="presentation" class="">...</li>
  <li role="presentation" class="">...</li>
</ul>
```

#### Suggested solution:

Add a compound box-shadow behind them.

```css
.paragraph-hero_slider .slick-dots li {
  box-shadow: 1px 1px 3px #000, -1px -1px 4px #000, 0 0 5px #000
  border-radius: 10px;
}
```

End result should have a fuzzy outline like this:

![fixed contrast indicators](assets/slide-indicator-low-contrast-fix.png)

<br>

---

<br>


### Button low contrast text indistinguishable from background


__Visual location:__

![Low contrast slide button](assets/low-contrast-button-1.png)

__HTML location:__

```html
<button class="btn white clear control-slider playing">Slider</button>
```

#### Suggested solution:

Give button some type of background color (can have `opacity`) so the white text is distinguishable from any background image.

<br>

---

### Background images are missing `background-color` attribute

[Automated tools like WAVE](https://wave.webaim.org/report#/https://dukehealthgme.epicenter1.com/) test for color contrast. The tools are not perfect.  Even though the text does have adequate contrast, it will throw an error for being low contrast without a dark `background-color` behind the image. 

Even though we know it is ok, we need to avoid throwing false positives anyway to stay out of legal problems.

Visually it will not change, because the `background-color` is behind the image.

__Visual location:__

![missing bg color](assets/low-contrast-missing-bg-color.png)

__CSS:__

```css
.paragraph-feature_image .module-image-banner {
  background-position: top center;
}
```

#### Suggested solution:

Add a hidden dark color behind the background image to avoid automated tool false positives.

`+` = add

```css
.paragraph-feature_image .module-image-banner {
  background-position: top center;
+  background-color: #333;
}
```


