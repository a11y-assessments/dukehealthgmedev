# Dukehealthgme.epicenter1.com Assessment

__<https://dukehealthgme.epicenter1.com/>__


__Screenshot:__

![Screenshot of this website](assets/dukehealthgme-epicenter1-com-_screenshot.png)

# Index of accessibility issues

### 08/05/2021 

Critical:

1. [Main menu issues](main-menu.html) [difficult]

Major:

1. [Color contrast issues](color-contrast.html) [easy]

1. [Missing keyboard focus](missing-keyboard-focus.html) [easy]

1. [Search form markup issues](form-markup.html) [medium]

1. [Logo missing accessible site name](link-name.html) [easy]

1. [Expand collapse widget issues](expand-collapse.html) [medium]


Minor:

1. [HTML semantics](innaccurate-html-semantics.html) [medium]

1. [ARIA Landmark issues](landmarks.html) [easy]
